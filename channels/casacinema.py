from bs4 import BeautifulSoup

from models.show import Show
from models.video import Video
from modules import urlresolver, scrapertools
from modules.helper import ShowType
import logging
import re

logger = logging.getLogger(__name__)

class Casacinema:
    def __init__(self):
        self.host = "http://www.casacinema.uno"
        self.headers = {
            'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; rv:38.0) Gecko/20100101 Firefox/38.0',
            'Referer': self.host
        }
        self.session = scrapertools.open_session(self.host)

    def search(self, text: str, target: str):
        if target == ShowType.MOVIE:
            return self.list_movies(text)
        else:
            return self.list_tvshow(text)

    def find_episode(self, show_url, season=1, episode=1):
        tv_show_page, _ = scrapertools.download_page(show_url, session=self.session, headers=self.headers)
        soup = BeautifulSoup(tv_show_page, 'html.parser')
        links = soup.find_all('a')
        matching_links = [x['href'] for x in links if self.contains_episode_str(x['href'], season, episode)]

        item_list = []
        for link in matching_links:
            original_url = link
            try:
                original_url = urlresolver.transform_url(original_url, session=self.session)
                if original_url and urlresolver.is_valid(original_url):
                    res_video = Video(original_url, 'casacinema', 'SD')
                    item_list.append(res_video)
            except:
                continue

        # close session
        scrapertools.close_session(self.host, self.session)

        # return items
        return item_list

    def list_tvshow(self, text):
        text = str(text).replace(" ", "+")
        search_url = "%s/?s=%s" % (self.host, text)
        try:
            item_list = []
            search_page, _ = scrapertools.download_page(search_url, session=self.session, headers=self.headers)
            soup = BeautifulSoup(search_page, 'html.parser')
            main_box = soup.find("ul", {"class": "posts"})
            found_items = main_box.findAll("li")
            for fitem in found_items:
                url = fitem.a.get('href')
                title = (fitem.find("div", {"class": "title"})).text
                if 'serie-tv' in url:
                    show_page = Show(ShowType.TV, title, url)
                    item_list.append(show_page)
            return item_list
        except Exception as e:
            logger.exception("casacinema")
            return []

    def list_movies(self, text: str):
        text = str(text).replace(" ", "+")
        search_url = "%s/?s=%s" % (self.host, text)
        try:
            item_list = []
            search_page, _ = scrapertools.download_page(search_url, session=self.session, headers=self.headers)
            soup = BeautifulSoup(search_page, 'html.parser')
            main_box = soup.find("ul", {"class": "posts"})
            found_items = main_box.findAll("li")
            for fitem in found_items:
                url = fitem.a.get('href')
                title = (fitem.find("div", {"class": "title"})).text
                if 'serie-tv' not in url:
                    show_page = Show(ShowType.MOVIE, title, url)
                    item_list.append(show_page)
            return item_list
        except Exception as e:
            logger.exception("casacinema")
            return []

    def find_movie(self, url):
        movie_page, _ = scrapertools.download_page(url, session=self.session, headers=self.headers)
        soup = BeautifulSoup(movie_page, 'html.parser')
        sources = soup.findAll('iframe', {"class": "embed-responsive-item"})
        link_list = [x.get('src') for x in sources]
        linkbox_list = [x.get('href') for x in soup.find("div", id="links").findAll("a")]

        total = link_list + linkbox_list

        item_list = []
        for link in total:
            original_url = link
            try:
                original_url = urlresolver.transform_url(original_url, session=self.session)
                if urlresolver.is_valid(original_url):
                    res_video = Video(original_url, 'casacinema', 'SD')
                    item_list.append(res_video)
            except:
                continue

        # close session
        scrapertools.close_session(self.host, self.session)

        # return items
        return item_list

    def contains_episode_str(self, link_src, season, episode):
        regex = r"S?0*(\d+)?[xE]0*(\d+)"
        matches = re.findall(regex, link_src, re.MULTILINE)
        for match in matches:
            matching_season, matching_episode = match
            if matching_season and matching_episode:
                if int(matching_season) == season and int(matching_episode) == episode:
                    return True
        return False
