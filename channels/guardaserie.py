from models.show import Show
from models.video import Video
from modules import scrapertools
import logging
from bs4 import BeautifulSoup

logger = logging.getLogger(__name__)

from models.enums.showtype import ShowType
from modules import urlresolver
from models import channel


class Guardaserie(channel.Channel):
    def __init__(self):
        self.host = "https://www.guardaserie.media"
        self.headers = {
            'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; rv:38.0) Gecko/20100101 Firefox/38.0'
        }
        self.session = scrapertools.open_session(self.host)

    def search(self, text, target):
        if target == ShowType.MOVIE:
            return self.list_movies(text)
        else:
            return self.list_tvshow(text)

    def find_movie(self, show_url):
        raise NotImplementedError()

    def list_movies(self, text):
        raise NotImplementedError()

    def find_episode(self, show_url, season=1, episode=1):
        data, _ = scrapertools.download_page(show_url, session=self.session, headers=self.headers)
        soup = BeautifulSoup(data, 'html.parser')

        results = []
        episode_box = soup.find('span', {'meta-stag': str(season), 'meta-ep': str(episode)})

        if not episode_box: return []
        title = episode_box.find_previous('div', {'class': 'number-episodes-on-img'}).text.lower()

        isSub = 'sub-' in title or 'sub ' in title

        # it is the right season
        url = episode_box.get('meta-embed')
        #url2 = episode_box.get('meta-embed2') # removed because unsupported services

        for url in [url]:
            if not url:
                continue
            url = urlresolver.transform_url(url, session=self.session)
            if urlresolver.is_valid(url):
                extracted_video = Video(url, 'guardaserie', is_sub=isSub)
                results.append(extracted_video)

        # close session
        scrapertools.close_session(self.host, self.session)

        return results

    def list_tvshow(self, text):
        search_url = "%s/?s=%s" % (self.host, text)
        data, _ = scrapertools.download_page(search_url, session=self.session, headers=self.headers)
        soup = BeautifulSoup(data, 'html.parser')
        shows = soup.findAll('a', {'class': 'box-link-serie'})
        results = []
        for show_box in shows:
            url = show_box['href']
            title = show_box.find('p', {'class': 'strongText'}).text
            show_page = Show(ShowType.TV, title, url)
            results.append(show_page)
        return results
