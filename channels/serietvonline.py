from bs4 import BeautifulSoup

from models.show import Show
from models.video import Video
from modules import urlresolver, scrapertools
from modules.helper import ShowType
import logging
import re

logger = logging.getLogger(__name__)

class Serietvonline:
    def __init__(self):
        self.host = "https://serietvonline.monster"
        self.headers = {
            'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; rv:38.0) Gecko/20100101 Firefox/38.0',
            'Referer': self.host
        }
        self.session = scrapertools.open_session(self.host)

    def search(self, text, target):
        if target == ShowType.MOVIE:
            return self.list_movies(text)
        else:
            return self.list_tvshow(text)

    def find_movie(self, show_url):
        tv_show_page, _ = scrapertools.download_page(show_url, session=self.session, headers=self.headers)
        soup = BeautifulSoup(tv_show_page, 'html.parser')
        link_src = soup.find('link', {'rel': 'shortlink'})
        show_id = link_src.get('href').split('=')[-1]

        links = soup.find_all('a')
        matching_links = [x for x in links if 'vcrypt' in x['href']]

        item_list = []
        for redirected_link in matching_links:
            try:
                redirected_link = urlresolver.transform_url(redirected_link, session=self.session)
            except:
                pass

            # check if valid
            if redirected_link and urlresolver.is_valid(redirected_link):
                extracted_video = Video(redirected_link, 'serietvonline')
                item_list.append(extracted_video)

        # close session
        scrapertools.close_session(self.host, self.session)

        # return items
        return item_list

    def list_movies(self, text):
        text = str(text).replace(" ", "+")
        search_url = "%s/?s=%s" % (self.host, text)
        item_list = []
        try:
            search_page, _ = scrapertools.download_page(search_url, session=self.session, headers=self.headers)
            soup = BeautifulSoup(search_page, 'html.parser')
            show_boxes = soup.find_all("div", {"class": "imagen"})
            for show_box in show_boxes:
                url = show_box.find('a').get('href')
                title = show_box.find('img').get('alt')
                if '/film/' in url:
                    show_page = Show(ShowType.TV, title, url)
                    item_list.append(show_page)
            return item_list
        except Exception as e:
            logger.exception(self.host)
            return item_list

    def find_episode(self, show_url, season=1, episode=1):
        tv_show_page, _ = scrapertools.download_page(show_url, session=self.session, headers=self.headers)
        soup = BeautifulSoup(tv_show_page, 'html.parser')

        links = soup.find_all('a')
        links = [x for x in links if 'vcrypt' in x['href']]
        matching_links = [x['href'] for x in links if x.get('title') and self.contains_episode_str(x['title'], season, episode)]

        item_list = []
        for redirected_link in matching_links:

            try:
                redirected_link = urlresolver.transform_url(redirected_link, session=self.session)
            except:
                pass

            # check if valid
            if redirected_link and urlresolver.is_valid(redirected_link):
                extracted_video = Video(redirected_link, 'serietvonline')
                item_list.append(extracted_video)

        new_item_list = [item_list[0]]
        if len(item_list) > 1:
            new_item_list.append(item_list[-1])

        # close session
        scrapertools.close_session(self.host, self.session)

        # return items
        return new_item_list

    def list_tvshow(self, text):
        text = str(text).replace(" ", "+")
        search_url = "%s/?s=%s" % (self.host, text)
        item_list = []
        try:
            search_page, _ = scrapertools.download_page(search_url, session=self.session, headers=self.headers)
            soup = BeautifulSoup(search_page, 'html.parser')
            show_boxes = soup.find_all("div", {"class": "imagen"})
            for show_box in show_boxes:
                url = show_box.find('a').get('href')
                title = show_box.find('img').get('alt')
                if '/film/' not in url:
                    show_page = Show(ShowType.TV, title, url)
                    item_list.append(show_page)
            return item_list
        except Exception as e:
            logger.exception(self.host)
            return item_list

    def contains_episode_str(self, link_src, season, episode):
        regex = r"S?0*(\d+)?[xE]0*(\d+)"
        matches = re.findall(regex, link_src, re.MULTILINE)
        for match in matches:
            matching_season, matching_episode = match
            if matching_season and matching_episode:
                if int(matching_season) == season and int(matching_episode) == episode:
                    return True
        return False
