from bs4 import BeautifulSoup

from models.show import Show
from models.video import Video
from modules import helper, scrapertools
import logging

logger = logging.getLogger(__name__)

from models.enums.showtype import ShowType
from models import channel


class AnimeUnity(channel.Channel):
    def __init__(self):
        self.host = "https://animeunity.it/"
        self.headers = {
            'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; rv:38.0) Gecko/20100101 Firefox/38.0',
            'Referer': self.host
        }
        self.session = scrapertools.open_session(self.host)

    def search(self, text, target):
        if target == ShowType.MOVIE:
            return self.list_movies(text)
        else:
            return self.list_tvshow(text)

    def find_movie(self, show_url):
        html_doc, _ = scrapertools.download_page(show_url, session=self.session, headers=self.headers)
        soup = BeautifulSoup(html_doc, 'html.parser')
        title = soup.find('h1', {'class': 'cus_title'}).text.split('-')[0]
        player = soup.find('div', {'id': 'player'})
        item_list = []
        if player:
            video_url = player.find('source').get('src')
            res_video = Video(video_url, 'animeunity', 'SD', title != '(ITA)')
            item_list.append(res_video)
        return item_list

    def list_movies(self, text):
        try:
            search_url = self.host + "anime.php?c=archive"
            data = {
                "query": text,
                "type": "Movie",
                "year": "(.*)",
                "status": "(.*)"
            }
            html_doc, _ = scrapertools.download_page(search_url, request_type="POST", data=data, headers=self.headers)
            soup = BeautifulSoup(html_doc, 'html.parser')
            found_items = soup.find_all("div", {'class': "archive-card"})
            item_list = []
            for item in found_items:
                show_title = item.find("h6", {'class': 'card-title'})
                show_url = self.host + str(item.find("a").get('href'))
                title = show_title.text.replace("\n", "").strip()
                matching = helper.compare_strings(text, title)
                if matching:
                    show_page = Show(ShowType.MOVIE, title, show_url)
                    item_list.append(show_page)
            return item_list

        except Exception:
            logger.exception("animeunity")
            return []

    def list_tvshow(self, text):
        try:
            search_url = self.host + "anime.php?c=archive"
            data = {
                "query": text,
                "type": "TV",
                "year": "(.*)",
                "status": "(.*)"
            }
            html_doc, _ = scrapertools.download_page(search_url, request_type="POST", data=data, headers=self.headers)
            soup = BeautifulSoup(html_doc, 'html.parser')
            found_items = soup.find_all("div", {'class': "archive-card"})
            item_list = []
            for item in found_items:
                show_title = item.find("h6", {'class': 'card-title'})
                show_url = self.host + str(item.find("a").get('href'))
                title = show_title.text.replace("\n", "").strip()
                matching = helper.compare_strings(text, title)
                if matching:
                    show_page = Show(ShowType.TV, title, show_url)
                    item_list.append(show_page)
            return item_list

        except Exception:
            logger.exception("animeunity")
            return []

    def find_episode(self, show_url, season=1, episode=1):
        try:
            html_doc, _ = scrapertools.download_page(show_url, session=self.session, headers=self.headers)
            soup = BeautifulSoup(html_doc, 'html.parser')
            episodes = soup.find_all("a", {'class': "ep-button"})

            item_list = []
            for e in episodes:
                episode_number = e.text
                if episode_number == str(episode):
                    episode_url = self.host + e.get('href')
                    html_doc, _ = scrapertools.download_page(episode_url, session=self.session, headers=self.headers)
                    soup = BeautifulSoup(html_doc, 'html.parser')
                    title = soup.find('h1', {'class': 'cus_title'}).text.split('-')[0]
                    player = soup.find('div', {'id': 'player'})
                    if player:
                        video_url = player.find('source').get('src')
                        res_video = Video(video_url, 'animeunity', 'SD', title != '(ITA)')
                        item_list.append(res_video)
                        break

            # close session
            scrapertools.close_session(self.host, self.session)

            return item_list
        except Exception as e:
            logger.exception("animeunity")
            return []
