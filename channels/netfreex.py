from typing import List

from bs4 import BeautifulSoup

from models.show import Show
from models.video import Video
from modules import helper, scrapertools
import logging

logger = logging.getLogger(__name__)

from models.enums.showtype import ShowType
from modules import urlresolver
from models import channel


class Netfreex(channel.Channel):
    def __init__(self):
        self.host = "https://www.netfreex.online"
        self.headers = {
            'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; rv:38.0) Gecko/20100101 Firefox/38.0',
            'Referer': self.host
        }
        self.session = scrapertools.open_session(self.host)

    def search(self, text, target):
        if target == ShowType.MOVIE:
            return self.list_movies(text)
        else:
            return self.list_tvshow(text)

    def find_movie(self, show_url):
        return self._find_link(show_url)

    def list_movies(self, text):
        item_list = []
        try:
            search_url = self.host + "/?s=" + text.replace(' ', '+')
            html_doc, _ = scrapertools.download_page(search_url, session=self.session, headers=self.headers)
            soup = BeautifulSoup(html_doc, 'html.parser')
            found_items = soup.find_all("div", {'class': "title"})
            for item in found_items:
                tmp = item.find("a")
                url = tmp.get('href')
                title = tmp.text.replace("\n", "").strip()
                matching = helper.compare_strings(text, title)
                if matching:
                    show_page = Show(ShowType.MOVIE, title, url)
                    item_list.append(show_page)
            return item_list
        except Exception:
            logger.exception("netfreex")
            return item_list

    def list_tvshow(self, text):
        item_list = []
        try:
            search_url = self.host + "/?s=" + text.replace(' ', '+')
            html_doc, _ = scrapertools.download_page(search_url, session=self.session, headers=self.headers)
            soup = BeautifulSoup(html_doc, 'html.parser')
            found_items = soup.find_all("div", {'class': "result-item"})
            for item in found_items:
                if item.find('span', {'class': 'tvshows'}):
                    tmp = item.find("div", {'class': 'title'}).find('a')
                    url = tmp.get('href')
                    title = tmp.text.replace("\n", "").strip()
                    matching = helper.compare_strings(text, title)
                    if matching:
                        show_page = Show(ShowType.TV, title, url)
                        item_list.append(show_page)
            return item_list
        except Exception:
            logger.exception("netfreex")
            return item_list

    def find_episode(self, show_url, season=1, episode=1):
        try:
            html_doc, _ = scrapertools.download_page(show_url, session=self.session, headers=self.headers)
            soup = BeautifulSoup(html_doc, 'html.parser')
            episodes = soup.find_all("div", {'class': "episodiotitle"})
            episodes_urls: List[str] = []
            for e in episodes:
                episode_url = e.find('a').get('href')
                episodes_urls.append(episode_url)

            to_search = str(season) + 'x' + str(episode)
            matching_urls = list(filter(lambda x: to_search in x, episodes_urls))
            if matching_urls:
                matching_url = matching_urls[0]
                return self._find_link(matching_url)
        except Exception as e:
            logger.exception("serietvu")
            return []

    def _find_link(self, show_url: str) -> List[Video]:
        item_list = []
        try:
            html_doc, _ = scrapertools.download_page(show_url, session=self.session, headers=self.headers)
            soup = BeautifulSoup(html_doc, 'html.parser')
            server_options = soup.find_all("li", {'class': "dooplay_player_option"})
            for option in server_options:
                data_nume = option.get('data-nume')
                data_post = option.get('data-post')
                data_type = option.get('data-type')
                quality = option.find('span', {'class': 'title'}).text
                request_direct_url = "https://www.netfreex.online/wp-admin/admin-ajax.php"
                data = {
                    'action': 'doo_player_ajax',
                    'post': data_post,
                    'nume': data_nume,
                    'type': data_type
                }
                html_doc, _ = scrapertools.download_page(request_direct_url, data=data, request_type="POST",
                                                         session=self.session, headers=self.headers)
                soup = BeautifulSoup(html_doc, 'html.parser')
                requested_video_url = soup.find("iframe").get('src')

                try:
                    requested_video_url = urlresolver.transform_url(requested_video_url, session=self.session)
                    if requested_video_url is not None and urlresolver.is_valid(requested_video_url):
                        extracted_video = Video(requested_video_url, 'netfreex', quality=quality)
                        item_list.append(extracted_video)
                except:
                    continue

            return item_list
        except Exception as e:
            logger.exception("netfreex")
            return item_list
