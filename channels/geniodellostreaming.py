from bs4 import BeautifulSoup
import logging

from models.enums.showtype import ShowType
from models.show import Show
from models.video import Video
from modules import urlresolver, scrapertools
from models import channel
import requests

logger = logging.getLogger(__name__)


class Geniostreaming(channel.Channel):
    def __init__(self):
        self.host = "https://ilgeniodellostreaming.si/"
        self.headers = {
            'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; rv:38.0) Gecko/20100101 Firefox/38.0',
            'Referer': self.host
        }
        self.session = scrapertools.open_session(self.host)

    def search(self, text, target):
        if target == ShowType.MOVIE:
            return self.list_movies(text)
        else:
            return self.list_tvshow(text)

    def find_movie(self, show_url):
        html_doc, _ = scrapertools.download_page(show_url, session=self.session, headers=self.headers)
        soup = BeautifulSoup(html_doc, 'html.parser')

        results = []

        # Get from table
        resolution = 'SD'
        isSub = False

        tbody = soup.find("tbody")
        rows = tbody.findAll('tr')

        for row in rows:
            columns = row.findAll('td')
            isSub = 'sub-' in columns[-2].text.lower()
            resolutionText = columns[-3].text.lower()
            resolution = 'HD' if (
                    '1080' in resolutionText or '720' in resolutionText or 'hd' in resolutionText) else 'SD'
            first_step_link = row.find("a", {'class': "link_a"})
            first_step_link = first_step_link.get('href')

            # get second step link
            inner_html_doc, _ = scrapertools.download_page(first_step_link, session=self.session, headers=self.headers)
            soup = BeautifulSoup(inner_html_doc, 'html.parser')
            redirected_link = soup.find('a')

            if redirected_link:
                redirected_link = redirected_link.get('href')

                try:
                    redirected_link = urlresolver.transform_url(redirected_link, session=self.session)
                except:
                    pass

                # check if valid
                if urlresolver.is_valid(redirected_link):
                    extracted_video = Video(redirected_link, 'geniodellostreaming', is_sub=isSub, quality=resolution)
                    results.append(extracted_video)

        # Get main box
        main_iframe_link = self.get_main_iframe_link(html_doc)
        if main_iframe_link and urlresolver.is_valid(main_iframe_link):
            extracted_video = Video(main_iframe_link, 'geniodellostreaming', is_sub=isSub)
            results.append(extracted_video)

        # close session
        scrapertools.close_session(self.host, self.session)

        # return items
        return results

    def list_movies(self, text):
        try:
            text = text.replace(' ', '+')
            search_url = self.host + "/?s=" + text

            item_list = []

            html_doc, _ = scrapertools.download_page(search_url, session=self.session, headers=self.headers)
            soup = BeautifulSoup(html_doc, 'html.parser')
            found_items = soup.find_all("div", {'class': "thumbnail"})
            for item in found_items:
                url = item.find('a').get('href')
                title = item.find('img').get('alt')
                category = item.find('span').text

                if category.lower() != 'film':
                    continue

                # if '[Sub-ITA]' not in title and show_name in title.lower():
                show_page = Show(ShowType.MOVIE, title, url)
                item_list.append(show_page)

            return item_list
        except Exception as e:
            print(e)
            return []

    def list_tvshow(self, text):
        try:
            text = text.replace(' ', '+')
            search_url = self.host + "/?s=" + text

            item_list = []

            html_doc, _ = scrapertools.download_page(search_url, session=self.session, headers=self.headers)
            soup = BeautifulSoup(html_doc, 'html.parser')
            found_items = soup.find_all("div", {'class': "thumbnail"})
            for item in found_items:
                url = item.find('a').get('href')
                title = item.find('img').get('alt')
                category = item.find('span').text

                if category.lower() != 'tv':
                    continue

                # if '[Sub-ITA]' not in title and show_name in title.lower():
                show_page = Show(ShowType.TV, title, url)
                item_list.append(show_page)

            return item_list
        except Exception as e:
            print(e)
            return []

    def find_episode(self, show_url, season=1, episode=1):
        try:
            html_doc, _ = scrapertools.download_page(show_url, session=self.session, headers=self.headers)
            soup = BeautifulSoup(html_doc, 'html.parser')
            season_elements = soup.find_all("div", {'class': "se-c"})

            result = None
            for season_element in season_elements:
                for episode_element in season_element.find_all("li"):
                    numerando_box = (episode_element.find("div", {'class': "numerando"})).text
                    s, e = numerando_box.split(' - ')
                    s = int(s)
                    e = int(e)
                    if e == episode and s == season:
                        result = {'url': episode_element.find('a').get('href'), 'episode': e, 'season': s}
                        break

                if result is not None:
                    break

            results = []

            if result:
                video_page = result.get('url')
                html_doc, _ = scrapertools.download_page(video_page, session=self.session, headers=self.headers)
                soup = BeautifulSoup(html_doc, 'html.parser')
                requested_video_url = (soup.find("iframe", {'class': "metaframe"})).get('src')

                if 'igds' in requested_video_url:
                    # needs a second request to get the redirected url
                    try:
                        requested_video_url = self.session.get(requested_video_url, timeout=5).url
                    except requests.exceptions.Timeout:
                        return results

                try:
                    requested_video_url = urlresolver.transform_url(requested_video_url, session=self.session)
                except:
                    pass

                if urlresolver.is_valid(requested_video_url):
                    extracted_video = Video(requested_video_url, 'geniodellostreaming', is_sub= 'sub-ita' in show_url)
                    results.append(extracted_video)

                # close session
                scrapertools.close_session(self.host, self.session)

                # return items
                return results

            elif 'sub-ita' not in show_url:
                # no-results found
                # try sub-ita
                show_url = show_url[:-1]
                show_url += '-sub-ita/'
                return self.find_episode(show_url, season, episode)

        except Exception as e:
            logger.exception("geniostreaming")
            return []

    def get_main_iframe_link(self, html_doc):
        soup = BeautifulSoup(html_doc, 'html.parser')
        main_iframe = soup.find("iframe", {'class': 'rptss'})

        if main_iframe:
            main_iframe_src = main_iframe.get('src')

            try:
                main_iframe_src = urlresolver.transform_url(main_iframe_src, session=self.session)
            except:
                pass

            return main_iframe_src

        return None
