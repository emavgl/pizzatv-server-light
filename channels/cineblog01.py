import re

from bs4 import BeautifulSoup
import logging

from models.enums.showtype import ShowType
from models.show import Show
from models.video import Video
from modules import urlresolver, scrapertools
from models import channel

logger = logging.getLogger(__name__)


class Cineblog(channel.Channel):
    def __init__(self):
        self.host = "https://cb01.irish"
        self.headers = {
            'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; rv:38.0) Gecko/20100101 Firefox/38.0',
            'Referer': self.host
        }
        self.session = scrapertools.open_session(self.host)

    def search(self, text, target):
        if target == ShowType.MOVIE:
            return self.list_movies(text)
        else:
            return self.list_tvshow(text)

    def list_movies(self, text):
        search_url = "{0}/?s={1}".format(self.host, text.replace(' ', '+'))
        data, _ = scrapertools.download_page(search_url, session=self.session, headers=self.headers)
        soup = BeautifulSoup(data, 'html.parser')
        cards_titles = soup.find_all('h3', {'class': 'card-title'})

        item_list = []

        for card in cards_titles:
            a_node = card.find('a')
            scraped_title = a_node.text
            scraped_url = a_node.get('href')
            show_page = Show(ShowType.MOVIE, scraped_title, scraped_url)
            item_list.append(show_page)

        return item_list

    def find_episode(self, show_url, season=1, episode=1):
        itemlist = []

        data, _ = scrapertools.download_page(show_url, session=self.session, headers=self.headers)

        inserted_link = []
        soup = BeautifulSoup(data, 'html.parser')
        for table_body in soup.findAll('div', {'class': 'sp-body'}):
            for strong in table_body.findAll('p'):
                match = re.search(r'(\d{1,2}).(\d{1,2})', strong.text)
                if not match: continue
                s = int(match.group(1))
                e = int(match.group(2))
                if e == episode and s == season:
                    for link in strong.findAll('a', href=True, text='Mixdrop'):
                        if link['href'] not in inserted_link:
                            inserted_link.append(link['href'])
                            previous_header = link.find_previous('div', {'class': 'sp-head'})
                            is_sub = 'SUB' in previous_header.text if previous_header else False
                            info = {'channel': 'cineblog01', 'server': 'mixdrop', 'sub': is_sub, 'url': link['href'],
                                    'quality': 'SD'}
                            itemlist.append(info)

                    for link in strong.findAll('a', href=True, text='Wstream'):
                        if link['href'] not in inserted_link:
                            inserted_link.append(link['href'])
                            previous_header = link.find_previous('div', {'class': 'sp-head'})
                            is_sub = 'SUB' in previous_header.text if previous_header else False
                            info = {'channel': 'cineblog01', 'server': 'wstream', 'sub': is_sub, 'url': link['href'],
                                    'quality': 'SD'}
                            itemlist.append(info)

                    for link in strong.findAll('a', href=True, text='Akvideo'):
                        if link['href'] not in inserted_link:
                            inserted_link.append(link['href'])
                            previous_header = link.find_previous('div', {'class': 'sp-head'})
                            is_sub = 'SUB' in previous_header.text if previous_header else False
                            info = {'channel': 'cineblog01', 'server': 'Akvideo', 'sub': is_sub, 'url': link['href'],
                                    'quality': 'SD'}
                            itemlist.append(info)
                    break

        parsed = []
        for item in itemlist:
            try:
                extracted_url = self.extract_url(item['url'])
                extracted_url = urlresolver.transform_url(extracted_url, self.session)
                if extracted_url and urlresolver.is_valid(extracted_url):
                    extracted_video = Video(extracted_url, 'cineblog01', is_sub=item['sub'])
                    parsed.append(extracted_video)
            except Exception as e:
                logger.exception("cineblog01")
                continue

        # close session
        scrapertools.close_session(self.host, self.session)

        return parsed

    def list_tvshow(self, text):
        search_url = "{0}/serietv/?s={1}".format(self.host, text.replace(" ", "+"))
        data, _ = scrapertools.download_page(search_url, session=self.session, headers=self.headers)

        item_list = []
        soup = BeautifulSoup(data, 'html.parser')
        cards_titles = soup.find_all('h3', {'class': 'card-title'})

        for card in cards_titles:
            a_node = card.find('a')
            scraped_title = self._gettvshowtitle(a_node.text)
            scraped_url = a_node.get('href')
            show_page = Show(ShowType.TV, scraped_title, scraped_url)
            item_list.append(show_page)

        return item_list

    def _gettvshowtitle(self, unparsed_title):
        parts = unparsed_title.split("–")
        if not parts:
            return unparsed_title
        else:
            return parts[0].strip()

    def get_resolution(self, header_text):
        if '3D' in header_text:
            return '3D'

        if 'HD' in header_text:
            return 'HD'

        return 'SD'

    def find_movie(self, show_url):
        item_list = []

        # Download page
        data, _ = scrapertools.download_page(show_url, session=self.session, headers=self.headers)

        isSub = 'sub-ita' in show_url

        inserted_link = []

        soup = BeautifulSoup(data, 'html.parser')
        for link in soup.findAll('a', href=True, text='Mixdrop'):
            if link['href'] not in inserted_link:
                previous_header = link.find_previous('strong')
                res = self.get_resolution(previous_header.text)
                inserted_link.append(link['href'])
                info = {'channel': 'cineblog01', 'sub': isSub, 'server': 'mixdrop', 'url': link['href'],
                        'quality': res}
                item_list.append(info)

        soup = BeautifulSoup(data, 'html.parser')
        for link in soup.findAll('a', href=True, text='Wstream'):
            if link['href'] not in inserted_link:
                previous_header = link.find_previous('strong')
                res = self.get_resolution(previous_header.text)
                inserted_link.append(link['href'])
                info = {'channel': 'cineblog01', 'sub': isSub, 'server': 'wstream', 'url': link['href'], 'quality': res}
                item_list.append(info)

        soup = BeautifulSoup(data, 'html.parser')
        for link in soup.findAll('a', href=True, text='Akvideo'):
            if link['href'] not in inserted_link:
                previous_header = link.find_previous('strong')
                res = self.get_resolution(previous_header.text)
                inserted_link.append(link['href'])
                info = {'channel': 'cineblog01', 'sub': isSub, 'server': 'akvideo', 'url': link['href'],
                        'quality': res}
                item_list.append(info)

        parsed = []
        for item in item_list:
            try:
                extracted_url = self.extract_url(item['url'])
                extracted_url = urlresolver.transform_url(extracted_url, self.session)
                if extracted_url and urlresolver.is_valid(extracted_url):
                    extracted_video = Video(extracted_url, 'cineblog01', is_sub=item['sub'])
                    parsed.append(extracted_video)
            except Exception as e:
                logger.exception("cineblog01")
                continue

        # close session
        scrapertools.close_session(self.host, self.session)

        return parsed

    def extract_url(self, url: str) -> str:
        """
        Try to get the direct url to the video from swzz and others
        :param url: url to pass es. http://swzz.xyz/link/pN7m4/
        :return: direct url to video hosting
        """
        data, response = scrapertools.download_page(url, session=self.session, headers=self.headers, proxy=False)
        url = response.url

        soup = BeautifulSoup(data, 'html.parser')
        node = soup.find("a", {'class': 'link'})

        if node:
            url = node['href'] # vcrypt link here

        return url
