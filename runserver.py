# import standard libraries

# Setup logging
from typing import List, Tuple

from models.searchresult import SearchResult
from models.video import Video
from modules import clogger

logger = clogger.get_logger(__name__)

# import application modules
import json
from channels import cineblog01, serietvonline
from channels import geniodellostreaming
from channels import serietvu
from channels import guardaserie
from channels import animeunity
from channels import casacinema

from modules import tmdb
from modules import helper
import concurrent.futures
from modules.helper import ShowType, compare_strings
from functools import reduce

# import third party libraries
from flask import Flask, g
from flask_cors import CORS

app = Flask(__name__)
CORS(app)  # enable cors

from flask import request


@app.route("/movie")
def movie():
    """
    Entry point for movies
    params:
    - showID (number): number tmdb show id
    - title (string): optional
    """
    show_id = helper.convert_to_str(request.args.get('showId'))
    title = helper.convert_to_str(request.args.get('title'))
    logger.info("[/movie] show_id: {0}; title: {1}".format(show_id, title))

    info = tmdb.get_info_by_id(show_id, ShowType.MOVIE)

    results = []
    if title and not compare_strings(title, info['title']):
        # title is specified and differes from the original one
        title = helper.clean(title)
        results = search_movie(title)
    else:
        # search using the title from tmdb
        title = helper.clean(info['title'])
        original_title = helper.clean(info['original_title'])

        results = search_movie(title)
        if len(results) == 0 and not compare_strings(title, original_title):
            # if no results and the title is different from the original title
            results = search_movie(original_title)

    results_no_duplicates = helper.remove_duplicates(results)

    res = {'results': results_no_duplicates}
    json_res = json.dumps(res, default=lambda x: x.__dict__)
    return json_res, 200, {'Content-Type': 'application/json'}


@app.route("/toplist")
def toplist():
    """
    Return toplists of the target
    - target (tv or movie)
    - showID (string): if specified, use reccomandation
    """
    target = helper.convert_to_str(request.args.get('target'))
    target = helper.string_to_showtype(target)
    show_id = helper.convert_to_str(request.args.get('showId'))
    page = helper.convert_to_str(request.args.get('page'))
    logger.info("[/toplist] show_id: {0}; page: {1}; target: {2}".format(show_id, page, target))
    res = []
    if show_id:
        res = tmdb.get_suggested_shows(target, show_id)
    else:
        res = tmdb.get_popular_shows(target, page)
    json_res = json.dumps(res, default=lambda x: x.__dict__)
    return json_res, 200, {'Content-Type': 'application/json'}


@app.route("/search")
def search():
    """
    Search title on tmdb
    """
    title = helper.convert_to_str(request.args.get('title'))
    title = title.encode('ascii', 'ignore')
    target = helper.convert_to_str(request.args.get('target'))
    target = helper.string_to_showtype(target)
    logger.info("[/search] target: {0}; title: {1}".format(target, title))
    search_info = tmdb.search(title, target)
    json_res = json.dumps(search_info, default=lambda x: x.__dict__)
    return json_res, 200, {'Content-Type': 'application/json'}


@app.route("/info")
def getInfo():
    """
    Get info from tmdb
    - title (string, optional)
    - showId (string)
    - target
    """
    title = helper.convert_to_str(request.args.get('title'))
    show_id = helper.convert_to_str(request.args.get('showId'))
    target = helper.convert_to_str(request.args.get('target'))
    target = helper.string_to_showtype(target)
    logger.info("[/info] target: {0}; title: {1}; show_id: {2}".format(target, title, show_id))
    search_info = tmdb.get_info_by_id(show_id, target)
    res = {'infoLabels': search_info}
    json_res = json.dumps(res, default=lambda x: x.__dict__)
    return json_res, 200, {'Content-Type': 'application/json'}


@app.route("/seasoninfo")
def handle_seasoninfo_req():
    """
    Returns info for the selected season
    """
    show_id = helper.convert_to_str(request.args.get('showId'))
    season = helper.convert_to_str(request.args.get('season'))
    logger.info("[/seasoninfo] season: {0}; show_id: {1}".format(season, show_id))
    season_info = tmdb.get_season_info(show_id, season)
    res = {'seasonInfo': season_info}
    json_res = json.dumps(res, default=lambda x: x.__dict__)
    return json_res, 200, {'Content-Type': 'application/json'}


@app.route("/tv")
def handle_tv_req():
    """
    Entry point for tv shows
    - showId
    - episode
    - season
    """
    show_id = helper.convert_to_str(request.args.get('showId'))
    episode = helper.convert_to_str(request.args.get('episode'))
    season = helper.convert_to_str(request.args.get('season'))
    logger.info("[/tv] episode: {0}; season: {1}; show_id: {2}".format(episode, season, show_id))

    # put information about the Show
    search_info = tmdb.get_info_by_id(show_id, ShowType.TV)

    # remove
    title = helper.clean(search_info['name'])
    original_name = helper.clean(search_info['original_name'])

    # run search
    results = search_tvshow(title, season, episode)
    if len(results) == 0 and not compare_strings(title, original_name):
        results = search_tvshow(original_name, season, episode)

    results_no_duplicates = helper.remove_duplicates(results)

    res = {'results': results_no_duplicates}
    json_res = json.dumps(res, default=lambda x: x.__dict__)
    return json_res, 200, {'Content-Type': 'application/json'}


def download_tvshow_from_source(search_target) -> Tuple[str, List[SearchResult]]:
    """
    Search and gets the links of an episode given a website (source)
    :param search_target:
    :return: list of links
    """
    name = search_target['name']
    service_class = search_target['service']
    title = search_target['title']
    season = search_target['season']
    episode = search_target['episode']

    search_results: List[SearchResult] = []
    try:
        service = service_class()
        search_list = service.search(title, ShowType.TV)
        for page in search_list:
            # 'game of thrones' in page['title'].lower() -- sad way to match game of thrones in all those sites
            # that have "Games of Thrones - Il Trono di spade" as original title
            if compare_strings(title, page.title) or 'game of thrones' in page.title.lower():
                res = service.find_episode(page.url, season, episode)
                search_result = SearchResult(page, res)
                if res:
                    search_results.append(search_result)
    except Exception as e:
        logger.exception("runserver - download_movie_from_source")

    return name, search_results


def search_tvshow(title: str, season: int, episode: int) -> List[SearchResult]:
    """
    Calls the method download_tvshow_from_source to run an episode research multi thread along
    multiple sources

    :param title: movie title
    :param season: season number
    :param episode: episode number
    :return: list of links
    """
    season = int(season)
    episode = int(episode)

    targets = [
        {'name': 'serietvu', 'service': serietvu.Serietvu, 'title': title, 'season': season, 'episode': episode},
        {'name': 'geniostreaming', 'service': geniodellostreaming.Geniostreaming, 'title': title, 'season': season,
         'episode': episode},
        {'name': 'cb01', 'service': cineblog01.Cineblog, 'title': title, 'season': season, 'episode': episode},
        #{'name': 'guardaserie', 'service': guardaserie.Guardaserie, 'title': title, 'season': season,
        # 'episode': episode},
        {'name': 'animeunity', 'service': animeunity.AnimeUnity, 'title': title, 'season': season,
         'episode': episode},
        {'name': 'casacinema', 'service': casacinema.Casacinema, 'title': title, 'season': season,
         'episode': episode},
        {'name': 'serietvonline', 'service': serietvonline.Serietvonline, 'title': title, 'season': season,
         'episode': episode},
    ]

    merge_results: List[SearchResult] = []
    number_of_videos = 0
    with concurrent.futures.ThreadPoolExecutor() as executor:
        for name, results in executor.map(download_tvshow_from_source, targets):
            videos_list = list(map(lambda x: len(x.videos), results))
            if videos_list:
                search_result_videos = reduce(lambda x, y: x + y, videos_list)
            else:
                search_result_videos = 0
            logger.info("[/search_tvshow] {0}-{1}x{2}; from {3} got {4} links".format(title, season, episode,
                                                                                      name, search_result_videos))
            number_of_videos += search_result_videos
            merge_results += results

    logger.info("[/search_tvshow] {0}-{1}x{2}; total links: {3}".format(title, season, episode, number_of_videos))

    return merge_results


def download_movie_from_source(search_target) -> Tuple[str, List[SearchResult]]:
    """
    Search and gets the links of a movie given a website (source)
    :param search_target:
    :return:
    """
    name = search_target['name']
    service_class = search_target['service']
    title = search_target['title']

    search_results: List[SearchResult] = []
    try:
        service = service_class()
        search_list = service.search(title, ShowType.MOVIE)
        for page in search_list:
            if compare_strings(title, page['title']):
                res: List[Video] = service.find_movie(page['url'])
                if res:
                    search_result = SearchResult(page, res)
                    search_results.append(search_result)
    except Exception:
        logger.exception("runserver - download_movie_from_source")

    return name, search_results


def search_movie(title: str) -> List[SearchResult]:
    """
    Calls the method download_movie_from_source to run a movie research multi thread along
    multiple sources
    :rtype: object
    """
    targets = [
        {'name': 'geniostreaming', 'service': geniodellostreaming.Geniostreaming, 'title': title},
        {'name': 'cb01', 'service': cineblog01.Cineblog, 'title': title},
        {'name': 'casacinema', 'service': casacinema.Casacinema, 'title': title}
    ]

    merge_results: List[SearchResult] = []
    number_of_videos = 0
    with concurrent.futures.ThreadPoolExecutor() as executor:
        for name, results in executor.map(download_movie_from_source, targets):
            videos_list = list(map(lambda x: len(x.videos), results))
            if videos_list:
                search_result_videos = reduce(lambda x, y: x + y, videos_list)
            else:
                search_result_videos = 0
            logger.info("[/search_movie] {0}; from {1} got {2} links".format(title, name, search_result_videos))
            number_of_videos += search_result_videos
            merge_results += results

    logger.info("[/search_movie] {0}; total links: {1}".format(title, number_of_videos))

    return merge_results


if __name__ == "__main__":
    app.run(host='0.0.0.0', port=4567)
