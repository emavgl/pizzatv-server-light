from unittest import TestCase
from channels import casacinema

class TestCasaCinema(TestCase):

    def test_find_movie(self):
        channel = casacinema.Casacinema()
        results = channel.list_movies("Share")
        movie_url = results[0].url
        video_urls = channel.find_movie(movie_url)
        print(video_urls)

    def test_list_movie(self):
        channel = casacinema.Casacinema()
        results = channel.list_movies("Share")
        self.assertTrue(len(results) > 0)
        movie_url = results[0].url
        self.assertIsNotNone(movie_url)

    def test_find_tv(self):
        channel = casacinema.Casacinema()
        results = channel.list_tvshow("Fear The Walking Dead")
        show_url = results[0].url
        self.assertEqual("https://www.casacinema.bid/fear-the-walking-dead-serie-tv-22/", show_url)

    def test_find_episode(self):
        channel = casacinema.Casacinema()
        show_url = "https://www.casacinema.bid/the-walking-dead-serie-tv-16/"
        season = 3
        episode = 5
        results = channel.find_episode(show_url, season, episode)
        self.assertTrue(len(results) > 0)