from unittest import TestCase
from channels import netfreex

class TestNetfreex(TestCase):

    def test_find_movie(self):
        channel = netfreex.Netfreex()
        results = channel.list_movies("selfie")
        movie_url = results[0].url
        video_urls = channel.find_movie(movie_url)
        print(video_urls)

    def test_list_movie(self):
        channel = netfreex.Netfreex()
        results = channel.list_movies("selfie")
        self.assertTrue(len(results) > 0)
        movie_url = results[0].url
        self.assertIsNotNone(movie_url)

    def test_list_tvshow(self):
        channel = netfreex.Netfreex()
        results = channel.list_tvshow("stranger things")
        self.assertTrue(len(results) > 0)
        movie_url = results[0].url
        self.assertIsNotNone(movie_url)

    def test_find_episode(self):
        channel = netfreex.Netfreex()
        results = channel.list_tvshow("the witcher")
        self.assertTrue(len(results) > 0)
        show_url = results[0].url
        results = channel.find_episode(show_url, 1, 1)
        self.assertTrue(len(results) > 0)
