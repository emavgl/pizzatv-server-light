from unittest import TestCase
from channels.animeunity import AnimeUnity

class TestAnimeUnity(TestCase):

    def test_list_tvshow(self):
        animeunity = AnimeUnity()
        results = animeunity.list_tvshow("Naruto")
        self.assertTrue(len(results) > 0)

    def test_find_episode(self):
        animeunity = AnimeUnity()
        results = animeunity.list_tvshow("Naruto")
        naruto_page = results[0].url
        r = animeunity.find_episode(naruto_page, 1, 60)
        self.assertTrue(len(r) > 0)

    def test_list_movie(self):
        animeunity = AnimeUnity()
        results = animeunity.list_movies("Boruto: Naruto the Movie")
        self.assertTrue(len(results) > 0)
        movie_url = results[0].url
        self.assertIsNotNone(movie_url)

    def test_find_movie(self):
        animeunity = AnimeUnity()
        results = animeunity.list_movies("Boruto: Naruto the Movie")
        movie_url = results[0].url
        movie_results = animeunity.find_movie(movie_url)
        self.assertTrue(len(movie_results) > 0)
        self.assertTrue('.mp4' in movie_results[0].url)
