from unittest import TestCase
from channels import serietvonline
from models.enums.showtype import ShowType

class TestSerietvonline(TestCase):

    def test_list_tvshow(self):
        channel = serietvonline.Serietvonline()
        results = channel.list_tvshow("Fear The Walking Dead")
        show_url = results[0].url
        title = results[0].title
        show_type = results[0].show_type
        self.assertEqual(ShowType.TV, show_type)
        self.assertEqual("Fear the Walking Dead", title)
        self.assertTrue("/fear-the-walking-dead/" in show_url)

    def test_find_episode(self):
        channel = serietvonline.Serietvonline()
        results = channel.list_tvshow("Fear The Walking Dead")
        season = 3
        episode = 5
        show_url = results[0].url
        results = channel.find_episode(show_url, season, episode)
        self.assertTrue(len(results) > 0)