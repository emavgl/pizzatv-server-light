
import unittest
import sys, json
from runserver import app
sys.path.append("../..")

class MovieAPI(unittest.TestCase):

	def setUp(self):
		self.app = app.test_client()
		self.app.testing = True

	def test_movie_given_id(self):
		req_url = '/movie?showId={0}'.format(335983)
		result = self.app.get(req_url)
		self.assertEqual(result.status_code, 200)
		res = json.loads(result.data.decode('utf-8'))
		self.assertTrue(len(res['videos']) > 0)

	def test_movie_given_title(self):
		req_url = '/movie?showId={0}&title={1}'.format(335983, "venom")
		result = self.app.get(req_url)
		self.assertEqual(result.status_code, 200)
		res = json.loads(result.data.decode('utf-8'))
		self.assertTrue(len(res['videos']) > 0)

	def test_movie_not_matching(self):
		req_url = '/movie?showId={0}'.format(399019)
		result = self.app.get(req_url)
		self.assertEqual(result.status_code, 200)
		res = json.loads(result.data.decode('utf-8'))
		print(res)
		self.assertTrue(len(res['videos']) > 0)


if __name__ == '__main__':
	unittest.main()
