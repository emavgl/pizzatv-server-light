
import unittest
import sys, json
from runserver import app
sys.path.append("../..")

class TmdbAPI(unittest.TestCase):

	def setUp(self):
		self.app = app.test_client()
		self.app.testing = True

	def test_toplist_tv(self):
		req_url = '/toplist?target=tv'
		result = self.app.get(req_url)
		self.assertEqual(result.status_code, 200)
		res = json.loads(result.data.decode('utf-8'))
		self.assertTrue(len(res) > 0)

	def test_toplist_movie(self):
		req_url = '/toplist?target=movie'
		result = self.app.get(req_url)
		self.assertEqual(result.status_code, 200)
		res = json.loads(result.data.decode('utf-8'))
		self.assertTrue(len(res) > 0)

	def test_suggested_tv(self):
		req_url = '/toplist?target=tv&showId={0}'.format(69050)
		result = self.app.get(req_url)
		self.assertEqual(result.status_code, 200)
		res = json.loads(result.data.decode('utf-8'))
		self.assertTrue(len(res) > 0)

	def test_suggested_movie(self):
		req_url = '/toplist?target=movie&showId={0}'.format(335983)
		result = self.app.get(req_url)
		self.assertEqual(result.status_code, 200)
		res = json.loads(result.data.decode('utf-8'))
		self.assertTrue(len(res) > 0)

	def test_season_info(self):
		req_url = '/seasoninfo?showId={0}&season={1}'.format(69050, 1)
		result = self.app.get(req_url)
		self.assertEqual(result.status_code, 200)
		res = json.loads(result.data.decode('utf-8'))
		self.assertTrue(res)

	def test_info_tv(self):
		req_url = '/info?showId={0}&target=tv'.format(69050)
		result = self.app.get(req_url)
		self.assertEqual(result.status_code, 200)
		res = json.loads(result.data.decode('utf-8'))
		self.assertTrue(res)

	def test_info_movie(self):
		req_url = '/info?showId={0}&target=movie'.format(335983)
		result = self.app.get(req_url)
		self.assertEqual(result.status_code, 200)
		res = json.loads(result.data.decode('utf-8'))
		self.assertTrue(res)

	def test_info_movie_with_title(self):
		req_url = '/info?showId={0}&target=movie&title={1}'.format(335983, "venom")
		result = self.app.get(req_url)
		self.assertEqual(result.status_code, 200)
		res = json.loads(result.data.decode('utf-8'))
		self.assertTrue(res)

	def test_info_movie_with_empty_title(self):
		req_url = '/info?showId={0}&target=movie&title={1}'.format(335983, " ")
		result = self.app.get(req_url)
		self.assertEqual(result.status_code, 200)
		res = json.loads(result.data.decode('utf-8'))
		self.assertTrue(res)

	def test_search_movie(self):
		req_url = '/search?title={0}&target=movie'.format("venom")
		result = self.app.get(req_url)
		self.assertEqual(result.status_code, 200)
		res = json.loads(result.data.decode('utf-8'))
		self.assertTrue(len(res) > 0)

	def test_search_tv(self):
		req_url = '/search?title={0}&target=tv'.format("the+100")
		result = self.app.get(req_url)
		self.assertEqual(result.status_code, 200)
		res = json.loads(result.data.decode('utf-8'))
		self.assertTrue(len(res) > 0)

if __name__ == '__main__':
	unittest.main()
