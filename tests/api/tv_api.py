
import unittest
import sys, json
from runserver import app
sys.path.append("../..")

class TvAPI(unittest.TestCase):

	def setUp(self):
		self.app = app.test_client()
		self.app.testing = True

	def test_tv(self):
		req_url = '/tv?showId={0}&episode={1}&season={2}'.format(69050, 1, 3)
		result = self.app.get(req_url)
		self.assertEqual(result.status_code, 200)
		res = json.loads(result.data.decode('utf-8'))
		print("Links: ", len(res['videos']))
		self.assertTrue(len(res['videos']) > 0)

	def test_not_found_episode(self):
		req_url = '/tv?showId={0}&episode={1}&season={2}'.format(69050, 1, 5)
		result = self.app.get(req_url)
		self.assertEqual(result.status_code, 200)
		res = json.loads(result.data.decode('utf-8'))
		print("Links: ", len(res['videos']))
		self.assertTrue(len(res['videos']) == 0)

if __name__ == '__main__':
	unittest.main()
