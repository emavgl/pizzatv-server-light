import unittest

import sys
sys.path.append("../..")

from modules.helper import compare_strings_debug

class CompareStrings(unittest.TestCase):

    def test_not_matching_1(self):
        str_1 = "Cattivissimo Me 3"
        str_2 = "Cattivissimo Me 2"
        self.assertFalse(compare_strings_debug(str_1, str_2))

    def test_matching_1(self):
        str_1 = "Spider-Man: Homecoming (2018)"
        str_2 = "SpiderMan Homecoming"
        self.assertTrue(compare_strings_debug(str_1, str_2))

    def test_not_matching_2(self):
        str_1 = "Spider-Man: Homecoming (2018)"
        str_2 = "The Amazing Spider-Man"
        self.assertFalse(compare_strings_debug(str_1, str_2))

    def test_not_matching_3(self):
        str_1 = "The Amazing Spider-Man"
        str_2 = "The Amazing Spider-Man 2 - Il potere di Electron"
        self.assertFalse(compare_strings_debug(str_1, str_2))


if __name__ == '__main__':
    unittest.main()