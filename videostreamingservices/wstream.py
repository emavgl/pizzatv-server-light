from urllib import parse

from bs4 import BeautifulSoup

from modules import scrapertools
import requests

from videostreamingservices.videostreamingservice import VideoStreamingService


class Wstream(VideoStreamingService):

    def transform(self, url: str, session: any) -> str:
        final_url = url
        if 'file_code' in url:
            query_file_code = parse.parse_qs(parse.urlparse(url).query)['file_code'][0]
            html_doc, r = scrapertools.download_page(url, session)
            soup = BeautifulSoup(html_doc, 'html.parser')
            input_tag = soup.find('input', {'value': query_file_code})
            if input_tag:
                id_value = input_tag.get('id')
                _, r = scrapertools.download_page(url, session, request_type="POST", data={id_value: query_file_code})
                final_url = r.url
        return final_url

    def is_valid(self, url: str) -> bool:
        try:
            r = requests.get(url, timeout=10)
            html_doc = r.text
            if 'was deleted by' in html_doc:
                return False
            return True
        except:
            return False
