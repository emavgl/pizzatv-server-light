import re
import base64
import requests
import logging

from videostreamingservices.videostreamingservice import VideoStreamingService

logger = logging.getLogger(__name__)


class Mixdrop(VideoStreamingService):

    def transform(self, url: str, session: any) -> str:
        return url.replace("/f/", "/e/")

    def is_valid(self, url: str) -> bool:
        try:
            r = requests.get(url, timeout=10)
            html_doc = r.text
            if r.status_code != 200 or 'we are sorry' in html_doc:
                return False
            return True
        except:
            logger.exception("mix_drop")
            return False
