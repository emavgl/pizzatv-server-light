import re
import base64
import requests
import logging

from videostreamingservices.videostreamingservice import VideoStreamingService

logger = logging.getLogger(__name__)


class Akvideo(VideoStreamingService):

    def transform(self, url: str, session: any) -> str:
        return url

    def is_valid(self, url: str) -> bool:
        try:
            r = requests.get(url, timeout=10)
            html_doc = r.text
            if r.status_code != 200 or 'file not found' in html_doc:
                return False
            return True
        except Exception as e:
            logger.exception("akvideo_invalid")
            return False
