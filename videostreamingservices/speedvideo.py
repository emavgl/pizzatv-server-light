import re
import base64
import requests
import logging

from videostreamingservices.videostreamingservice import VideoStreamingService

logger = logging.getLogger(__name__)


class Speedvideo(VideoStreamingService):

    def transform(self, url: str, session: any) -> str:
        return url

    def is_valid(self, url: str) -> bool:
        try:
            r = requests.get(url, timeout=10)
            html_doc = r.text
            if 'is expired' in html_doc.lower() or 'file was deleted' in html_doc.lower():
                return False
            return True
        except Exception as e:
            logger.exception("speed_video_isvalid")
            return False