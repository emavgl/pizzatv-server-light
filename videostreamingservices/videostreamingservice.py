from abc import ABC, abstractmethod


class VideoStreamingService(ABC):

    @abstractmethod
    def transform(self, url: str, session: any) -> str:
        pass

    @abstractmethod
    def is_valid(self, url: str) -> bool:
        pass