requests
Flask
BeautifulSoup4
cfscrape>=2.0.8
gunicorn
pymysql
flask-cors
cloudscraper>=1.2.24