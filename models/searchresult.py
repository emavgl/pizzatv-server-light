from typing import List

from models.show import Show
from models.video import Video


class SearchResult:
    show: Show
    videos: List[Video]

    def __init__(self, show: Show, videos: List[Video]):
        self.videos = videos
        self.show = show