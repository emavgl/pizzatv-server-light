class Proxy:
    ip_addr: str
    port: str
    code: str
    anonymity: str
    is_https: bool

    def __init__(self, ip_addr: str, port: str, code: str, anonymity: str, is_https: bool):
        self.is_https = is_https
        self.anonymity = anonymity
        self.code = code
        self.port = port
        self.ip_addr = ip_addr

    def __getitem__(self, item):
        return getattr(self, item)

    def to_url(self) -> str:
        if self.is_https:
            return "https://{}:{}".format(self.ip_addr, self.port)
        else:
            return "http://{}:{}".format(self.ip_addr, self.port)

    def to_dict(self) -> dict:
        proxy_dict = {'http': "http://ezcdnner-1:vvjvzyjolprq@p.webshare.io", 'https': "http://ezcdnner-1:vvjvzyjolprq@p.webshare.io"}
        return proxy_dict
