from models.enums.showtype import ShowType


class Show:

    title: str
    show_type: ShowType
    url: str

    def __init__(self, show_type: ShowType, title: str, url: str):
        self.title = title
        self.show_type = show_type
        self.url = url

    def __getitem__(self, item):
        return getattr(self, item)