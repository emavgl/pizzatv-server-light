from modules import scrapertools


class Video:

    url: str
    server: str
    channel: str
    quality: str
    is_sub: bool

    def __init__(self, url: str, channel: str, quality: str = 'SD', is_sub: bool = False):
        self.is_sub = is_sub
        self.quality = quality
        self.channel = channel
        self.server = scrapertools.get_website_name(url)
        self.url = url

    def __getitem__(self, item):
        return getattr(self, item)