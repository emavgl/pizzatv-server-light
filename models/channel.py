from models.enums.showtype import ShowType
import abc

from models.show import Show
from typing import List

from models.video import Video


class Channel(metaclass=abc.ABCMeta):

    def search(self, title: str, target: ShowType):
        """
        Search all the shows that match the title and target
        :param title: title of the show
        :param target: ShowType.MOVIE or ShowType.TV
        :return: list of matching pages
        """
        if target == ShowType.MOVIE:
            return self.list_movies(title)
        else:
            return self.list_tvshow(title)

    @abc.abstractmethod
    def list_tvshow(self, title: str) -> List[Show]:
        """
        Return a list of web page matching matching title
        :param title: title of the tv show
        """
        raise NotImplementedError()

    @abc.abstractmethod
    def list_movies(self, title: str) -> List[Show]:
        """
        Return a list of web page matching matching title
        :param title: title of the movie show
        """
        raise NotImplementedError()

    @abc.abstractmethod
    def find_episode(self, show_url: str, season: int = 1, episode: int = 1) -> List[Video]:
        """
        Get episode links
        :param show_url: web page of the show
        :param season: season number to search >= 0
        :param episode: episode number to search >= 0
        :return list of videos with links
        """
        raise NotImplementedError()

    @abc.abstractmethod
    def find_movie(self, show_url: str) -> List[Video]:
        """
        Get movie links
        :param show_url: web page of the show
        :return: list of videos with links
        """
        raise NotImplementedError()
