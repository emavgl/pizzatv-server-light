import requests
import json
from modules.helper import ShowType
from modules import helper

# Setup logging
from modules import clogger
logger = clogger.get_logger(__name__)

API_KEY = "62d33f3b98ca88f6c5ce8472ebb92b35"
SEARCH_MOVIE_URL = "https://api.themoviedb.org/3/search/movie"
SEARCH_TV_URL = "https://api.themoviedb.org/3/search/tv"
POSTER_BASE = "https://image.tmdb.org/t/p/w500"
POSTER_BASE_THUMBNAIL = "https://image.tmdb.org/t/p/w185"
BACKDROP_BASE = "https://image.tmdb.org/t/p/original"
BACKDROP_BASE_THUMBNAIL = "https://image.tmdb.org/t/p/w300"
TV_BASE = "https://api.themoviedb.org/3/tv/"
MOVIE_BASE = "https://api.themoviedb.org/3/movie/"


def create_show(show: dict):
    """
    Parse tmdb response and adjust things
    :param show:
    :return:
    """
    if show['poster_path']:
        show['poster_path_thumbnail'] = POSTER_BASE_THUMBNAIL + show['poster_path']
        show['poster_path'] = POSTER_BASE + show['poster_path']
    if show['backdrop_path']:
        show['backdrop_path_thumbnail'] = BACKDROP_BASE_THUMBNAIL + show['backdrop_path']
        show['backdrop_path'] = BACKDROP_BASE + show['backdrop_path']

    if 'title' not in show:
        show['title'] = show['name']

    return show


def search(title: str, target: ShowType):
    """
    Search tmdb shows from title
    :param title:
    :param target:
    :return:
    """
    try:
        BASE_URL = SEARCH_MOVIE_URL
        if target == ShowType.TV:
            BASE_URL = SEARCH_TV_URL

        # clean title
        title = helper.clean(title)

        # create payload
        payload = {
                    'api_key': API_KEY, 'language': 'it',
                    'query': title, 'page': 1
                  }

        # send request
        res = requests.get(BASE_URL, payload)
        dic_res = json.loads(res.text)

        # retrieve shows
        results = []

        if 'errors' in dic_res:
            return []

        if dic_res['total_results'] > 0:
            for res in dic_res['results']:
                results.append(create_show(res))

        return results
    except Exception as e:
        logger.exception('tmdb info')
        return []


def get_popular_shows(target: ShowType, page: str):
    """
    Query TMDB to get a list of popular shows
    :param target: ShowType enum
    :param page: TMDB page
    :return:
    """
    if not page:
        page = 1
    payload = {'api_key': API_KEY, 'language': 'it', 'page': page, 'region': 'IT'}
    url_path = MOVIE_BASE
    if target == ShowType.TV:
        url_path = TV_BASE
    url_path = url_path + "popular"
    res = requests.get(url_path, payload)
    popular = json.loads(res.text)
    popular = popular['results']
    item_list = []
    for pop in popular:
        item_list.append(create_show(pop))
    return item_list


def get_season_info(show_id: str, season: int) -> object:
    """
    Query TMDB to get season info object
    :param show_id: tmdb id
    :param season: season number to get inf
    :return:
    """
    url_path = TV_BASE
    url_path += show_id + "/season/" + str(season)
    payload = {'api_key': API_KEY, 'language': 'it'}
    res = requests.get(url_path, payload)
    seasonInfo = json.loads(res.text)
    episodeList = seasonInfo['episodes']

    if seasonInfo['poster_path']:
        seasonInfo['poster_path'] = POSTER_BASE + seasonInfo['poster_path']

    for episode in episodeList:
        if episode['still_path']:
            episode['still_path'] = BACKDROP_BASE + episode['still_path']
            episode['still_path_thumbnail'] = BACKDROP_BASE_THUMBNAIL + episode['still_path']
            episode['backdrop_path'] = episode['still_path']
            episode['backdrop_path_thumbnail'] = episode['still_path_thumbnail'] 

    return seasonInfo


def get_suggested_shows(target: ShowType, show_id: str):
    """
    Get suggested shows using showId as reference show
    :param target: ShowType enum movie or tv
    :param show_id: reference show
    :return:
    """
    url_path = MOVIE_BASE
    if target == ShowType.TV:
        url_path = TV_BASE
    payload = {'api_key': API_KEY, 'language': 'it'}
    url_path = url_path + show_id + "/recommendations"
    res = requests.get(url_path, payload)
    suggested = json.loads(res.text)
    suggested = suggested['results']
    item_list = []
    for pop in suggested:
        item_list.append(create_show(pop))
    return item_list


def get_info_by_title(title: str, target: ShowType):
    """
    Get TMDB entry information from title
    :param title: title to search
    :param target: target (tv or movie)
    :return: object with tmdb info or null
    """
    try:
        results = search(title, target)
        showid = (results[0])['id']
        return get_info_by_id(showid, target)
    except Exception:
        logger.exception('tmdb info')
        return None

def get_info_by_id(show_id: str, target: ShowType):
    """
    Get TMDB entry information from tmdb_id
    :param show_id: tmdb id
    :param target: target (tv or movie)
    :return: object with tmdb info or null
    """
    try:
        # get api url from target
        if target == ShowType.TV:
            url = TV_BASE + str(show_id)
            payload = {'api_key': API_KEY, 'language': 'it'}
        else:
            url = MOVIE_BASE + str(show_id)
            payload = {'api_key': API_KEY, 'language': 'it'}

        # request
        res = requests.get(url, payload)

        # parse response
        show_info = create_show(json.loads(res.text))

        # get complete youtube urls
        if target == ShowType.MOVIE:
            url = MOVIE_BASE + str(show_id) + "/videos"
            payload = {'api_key': API_KEY, 'language': 'it'}
            res = requests.get(url, payload)
            videoInfo = json.loads(res.text)
            show_info['videos'] = []
            for video in videoInfo['results']:
                if video['site'] == 'YouTube':
                    embed_url = 'https://www.youtube.com/embed/' + video['key']
                    show_info['videos'].append({'url': embed_url, 'id': video['key'], 'title': video['name']})
        
        return show_info
    except Exception as e:
        logger.exception('tmdb info')
        return None
