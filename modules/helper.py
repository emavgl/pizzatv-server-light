import difflib
import re
from typing import List

from models.enums.showtype import ShowType
from models.searchresult import SearchResult

from modules import clogger
logger = clogger.get_logger(__name__)


def _remove_text_inside_brackets(text, brackets="()[]"):
    """
    Remove brackets and the text inside
    :param text:
    :param brackets:
    :return:
    """
    count = [0] * (len(brackets) // 2) # count open/close brackets
    saved_chars = []
    for character in text:
        for i, b in enumerate(brackets):
            if character == b: # found bracket
                kind, is_close = divmod(i, 2)
                count[kind] += (-1)**is_close # `+1`: open, `-1`: close
                if count[kind] < 0: # unbalanced bracket
                    count[kind] = 0  # keep it
                else:  # found bracket to remove
                    break
        else: # character is not a [balanced] bracket
            if not any(count): # outside brackets
                saved_chars.append(character)
    return ''.join(saved_chars)


def convert_to_str(data):
    """
    Convert to unicode (utf8)
    :param data:
    :return:
    """
    if not data:
        return ''

    if not isinstance(data, str):
        data = data.decode('utf-8')
    return data


def clean(data: str) -> str:
    """
    Remove brackets, not alphanumeric char, encode ascii, lower, strip
    :param data:
    :return: cleaned string
    """
    text = convert_to_str(data)

    # remove brackets
    no_brack = _remove_text_inside_brackets(text)

    # replace any not alphanumeric char with space
    alphanum = re.sub('[^0-9a-zA-Z ]+', ' ', no_brack)

    # encode in ascii
    new_str = alphanum

    # make it lower
    new_str = new_str.lower()

    # strip
    new_str = new_str.strip()

    # remove double spaces
    new_str = new_str.replace("  ", " ")

    return new_str


def compare_strings_debug(str1, str2, margin=0.90):
    """
    Check if two strings refer to the same title
    :param str1: first title
    :param str2: second title
    :param margin: minimum percentage of equality
    :return: true if two strings are equal within a margin of 0.90, false otherwise
    """
    str1 = clean(str1)
    str2 = clean(str2)
    print(str1)
    print(str2)

    ratio = difflib.SequenceMatcher(None, str1, str2).ratio()
    print(ratio)

    # Compare also list of numbers in the way that
    # Cattivissimo Me 3 does not to match with Cattivissimo Me 2
    numbers1 = [int(s) for s in str1.split() if s.isdigit()]
    numbers2 = [int(s) for s in str2.split() if s.isdigit()]

    if ratio > margin and numbers1 == numbers2:
        return True

    return False

def compare_strings(str1, str2, margin=0.90):
    """
    Check if two strings refer to the same title
    :param str1: first title
    :param str2: second title
    :param margin: minimum percentage of equality
    :return: true if two strings are equal within a margin of 0.90, false otherwise
    """
    str1 = clean(str1)
    str2 = clean(str2)
    
    ratio = difflib.SequenceMatcher(None, str1, str2).ratio()

    # Compare also list of numbers in the way that
    # Cattivissimo Me 3 does not to match with Cattivissimo Me 2
    numbers1 = [int(s) for s in str1.split() if s.isdigit()]
    numbers2 = [int(s) for s in str2.split() if s.isdigit()]

    if ratio > margin and numbers1 == numbers2:
        return True

    return False


def is_contained_in(str1: str, str2: str):
    """
    Check if str1 is in str2
    :param str1:
    :param str2:
    :return:
    """
    str1 = clean(str1)
    str2 = clean(str2)
    return (str1 in str2)


def string_to_showtype(label: str):
    """
    Convert string to enum ShowType
    :param label:
    :return:
    """
    if label == 'tv':
        return ShowType.TV
    return ShowType.MOVIE


def remove_duplicates(search_results: List[SearchResult]) -> List[SearchResult]:
    """
    Remove duplicates video from a list of search results
    :return: List of search results without duplicates video
    """
    unique_search_results: List[SearchResult] = []
    seen_video_url: List[str] = []

    for search_result in search_results:
        unique_videos = []
        videos = search_result.videos

        for video in videos:
            if video.url not in seen_video_url:
                unique_videos.append(video)
                seen_video_url.append(video.url)

        if unique_videos:
            search_result.videos = unique_videos
            unique_search_results.append(search_result)

    return unique_search_results
