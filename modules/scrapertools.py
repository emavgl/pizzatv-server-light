﻿# -*- coding: utf-8 -*-
import os
import re
import random
from urllib.parse import urlparse
# import cfscrape
import cloudscraper
import pickle

# Setup logging
from bs4 import BeautifulSoup

from models.proxy import Proxy
from modules import clogger
logger = clogger.get_logger(__name__)

COOKIES_PATH = './cookies'

DEFAULT_HEADERS = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; rv:38.0) Gecko/20100101 Firefox/38.0'
}

if not os.path.exists(COOKIES_PATH):
    os.mkdir(COOKIES_PATH)

# 5 seconds
DEFAULT_TIMEOUT = 5


def download_page(url, session=None, request_type='get', data=None, use_cookies=True, headers=None, proxy=False):
    """
    Wrapper for HTTP requests using Request library
    """
    if headers is None:
        headers = DEFAULT_HEADERS

    s = session
    if not s:
        s = open_session(url, use_cookies)

    # requests
    if request_type == 'get':
        if proxy:
            random_proxy = get_random_proxy().to_dict()
            response = s.get(url, data=data, headers=headers, timeout=DEFAULT_TIMEOUT, proxies=random_proxy)
        else:
            response = s.get(url, data=data, headers=headers, timeout=DEFAULT_TIMEOUT)
    else:
        if proxy:
            random_proxy = get_random_proxy().to_dict()
            response = s.post(url, data=data, headers=headers, timeout=DEFAULT_TIMEOUT, proxies=random_proxy)
        else:
            response = s.post(url, data=data, headers=headers, timeout=DEFAULT_TIMEOUT)

    if session is None:
        close_session(url, s)

    return response.text, response


def save_cookies(host, session):
    """
    Save session's cookies for an host on file
    :param host: host's cookies
    :param session: requests' session
    :return: True
    """
    domain = urlparse(host)[1].replace("www.", "")
    path_cookies = os.path.join(COOKIES_PATH, domain + ".pkl")
    with open(path_cookies, 'wb+') as f:
        pickle.dump(session.cookies, f)
#    logger.debug("cookies for {} are stored at {}".format(host, path_cookies))
    return True


def load_cookies(host):
    """
    Load cookies for a given host
    :param host: host I'm interested in
    :return: cookies
    """
    domain = urlparse(host)[1].replace("www.", "")
    path_cookies = os.path.join(COOKIES_PATH, domain + ".pkl")
    if not os.path.isfile(path_cookies):
        return None

    with open(path_cookies, 'rb') as f:
        return pickle.load(f)


def open_session(host, use_cookies=True, proxy=False):
    """
    Open a new session
    """
    session = cloudscraper.create_scraper()

    if proxy:
        random_proxy = get_random_proxy()
        session.proxies.update(random_proxy.to_dict())

    cookies = load_cookies(host)

    if cookies and use_cookies:
        # load cookies
        session.cookies.update(cookies)
        logger.debug("new session: using stored cookies")
    else:
        # to load cookies and pass the cloudflare challenge if needed
        logger.debug("new session: no cookies loaded")
        _ = session.get(host, timeout=15).content

    return session


def close_session(host, session):
    """
    Save the cookies and close the session
    """
    # save cookies
    save_cookies(host, session)

    # close session
    session.close()

#    logger.debug("session is closed")


def get_header_from_response(url, header_to_get):
    content, response = download_page(url)
    return response.headers.get(header_to_get)


# Useful method for regex
def get_match(data, patron, index=0):
    matches = re.findall(patron, data, flags=re.DOTALL)
    return matches[index]


def find_single_match(data, patron, index=0):
    try:
        matches = re.findall(patron, data, flags=re.DOTALL)
        return matches[index]
    except:
        return ""


def find_multiple_matches(text, pattern):
    return re.findall(pattern, text, re.DOTALL)


def get_website_name(url: str) -> str:
    """
    Extract website name from url
    :param url: url from which extract the website name
    :return: website name
    """
    website_name_mapper = {
        "woof": "verystream",
    }
    hostname = urlparse(url).hostname
    website_name = (hostname.replace("www.", "").split("."))[0]
    if website_name in website_name_mapper:
        return website_name_mapper[website_name]
    else:
        return website_name


def get_random_proxy() -> Proxy:
    html_doc, _ = download_page("https://free-proxy-list.net/")
    soup = BeautifulSoup(html_doc, 'html.parser')
    proxylisttable = soup.find('table', {'id': 'proxylisttable'})
    rows = proxylisttable.find_all('tr')
    proxies = []
    for row in rows:
        cols = row.find_all('td')
        if cols:
            is_https = False if 'no' == cols[6].text else True
            proxy = Proxy(cols[0].text, cols[1].text, cols[2].text, cols[4].text, is_https)
            proxies.append(proxy)

    https_supported = [x for x in proxies if x.is_https]
    if https_supported:
        return random.choice(https_supported)
    else:
        return random.choice(proxies)
