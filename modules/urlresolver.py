# Setup logging
from modules import clogger, scrapertools
from videostreamingservices.akvideo import Akvideo
from videostreamingservices.mixdrop import Mixdrop
from videostreamingservices.speedvideo import Speedvideo
from videostreamingservices.vcrypt import Vcrypt
from videostreamingservices.wstream import Wstream

logger = clogger.get_logger(__name__)

sources = {'speedvideo': Speedvideo(), 'mixdrop': Mixdrop(), 'wstream': Wstream(), 'akvideo': Akvideo(), 'vcrypt': Vcrypt()}


def transform_url(url: str, session: any) -> str:
    """
    Given an intermediate video url, it returns the final url linking to the video
    :param url: intermediate video url
    :param session: current session
    :return: url linking directly to the video, None if the URL canno't be handle
    """
    website_name = scrapertools.get_website_name(url)
    if website_name in sources:
        source_module = sources[website_name]
        return source_module.transform(url, session)
    raise Exception('url ' + url + ' cannot be handle')


def is_valid(url: str) -> bool:
    """
    Checks if the link is valid and whether the video is still online
    :param url: to check its validity
    :return: boolean, true if valid, false otherwise (or exception)
    """
    # remove all those urls that ends with an archive extension (false video files)
    if '.rar' in url or '.zip' in url:
        return False
    try:
        website_name = scrapertools.get_website_name(url)
        if website_name in sources:
            source_module = sources[website_name]
            return source_module.is_valid(url)
        return False
    except Exception:
        logger.exception("url_resolver_is_valid")
        logger.info(url + ' is not a valid website')
        return False